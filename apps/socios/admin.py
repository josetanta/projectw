from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Socio, CuentaAhorro


# Register your models here.


class SocioModelResources(resources.ModelResource):
    class Meta:
        model = Socio


@admin.register(Socio)
class SocioModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = (
        'dni',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'fecha_nacimiento',
        'telefono',
        'email',
    )


class CuentaAhorroModelResources(resources.ModelResource):
    class Meta:
        model = CuentaAhorro


@admin.register(CuentaAhorro)
class CuentaAhorroModelAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = (
        'socio',
        'monto_cuenta',
        'fecha_retiro',
    )
