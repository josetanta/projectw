import os

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models

from ..usuario.models import User


# Create your models here.

# Create of Socio


class Socio(models.Model):
    ELECCION_GENERO = (
        ('F', 'Femenino'),
        ('M', 'Masculino'),
    )

    ESTADO = (
        (True, 'Activo'),
        (False, 'Desactivado'),
    )

    id = models.BigAutoField(primary_key=True)
    nombre = models.CharField(
        'Nombre(s)', max_length=100, blank=False, null=False)
    apellido_paterno = models.CharField(
        'Apellido Paterno', max_length=60, blank=False, null=False)
    apellido_materno = models.CharField(
        'Apellido Materno', max_length=60, blank=False, null=False)
    email = models.EmailField('Email', unique=True, blank=False, null=False)
    dni = models.CharField('DNI', blank=False, null=False, unique=True,
                           max_length=8, validators=[RegexValidator(r'^\d{0,10}$')])
    genero = models.CharField('Género', max_length=1,
                              blank=False, null=False, choices=ELECCION_GENERO)
    fecha_nacimiento = models.DateField('Fecha de nacimiento', auto_now=False)
    telefono = models.CharField('Teléfono', max_length=9, unique=False,
                                blank=True, null=True, validators=[RegexValidator(r'^\d{0,10}$')])
    estado = models.BooleanField('Estado', default=True, choices=ESTADO)
    fb = models.URLField('Facebook', unique=True, blank=True, null=True)
    # Atributtes of class Socio -> Data of location
    fecha_registro = models.DateTimeField(
        'Fecha de registro', auto_now_add=True)
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, unique=True, related_name='socio')

    class Meta:
        ordering = ['id']
        verbose_name = 'Socio'
        verbose_name_plural = 'Socios'
        db_table = 'socios'
        # app_label = 'wfinanciera'

    def __str__(self):
        return "Socio %s" % (self.dni)

    def clean(self):
        if self.email == os.environ.get('MAIL_ADMIN'):
            raise ValidationError('Este email no esta permitido.')

    def get_absolute_url(self):
        from django.shortcuts import reverse

        return reverse('socio:detallesocio', kwargs={'pk': self.id})

    @property
    def is_prestamos_max(self):
        return self.prestamos.count() == 3


class CuentaAhorro(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True)
    monto_cuenta = models.DecimalField(
        'Monto en Cuenta', max_digits=19, decimal_places=2, default=0)
    fecha_retiro = models.DateTimeField('Fecha de registro', auto_now=True)
    socio = models.OneToOneField(
        Socio, on_delete=models.CASCADE, unique=True, related_name='cuenta_ahorro')

    class Meta:
        db_table = 'cuenta_ahorros'
        # app_label = 'wfinanciera'

    def __str__(self):
        return "%d" % self.monto_cuenta

    def add_amount_loan_approved(self, amount):
        self.monto_cuenta += amount
        self.save()
