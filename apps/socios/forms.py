import os
from django import forms
from django.core.exceptions import NON_FIELD_ERRORS
from .models import Socio


class SocioForm(forms.ModelForm):
    class Meta:
        model = Socio

        fields = [
            'nombre',
            'apellido_paterno',
            'apellido_materno',
            'dni',
            'genero',
            'fecha_nacimiento',
            'telefono',
            'email',
            'fb',
        ]

        labels = {
            'nombre': 'Nombre(s)',
            'apellido_paterno': 'Apellido Paterno',
            'apellido_materno': 'Apellido Materno',
            'dni': 'DNI',
            'genero': 'Género',
            'fecha_nacimiento': 'Fecha de Nacimiento',
            'telefono': 'Número de celular',
            'email': 'Email',
            'fb': 'Facebook',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Digíte sus nombres'}),
            'apellido_paterno': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Digite su apellido paterno'}),
            'apellido_materno': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Digite su apellido materno'}),
            'dni': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Digite su Documento Nacional de Identidad',
                       'id': 'dni_socio'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'fecha_nacimiento': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Digite solo números'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Ejemplo socio@mail.com'}),
            'fb': forms.URLInput(attrs={'class': 'form-control', 'placeholder': 'Copie la URL de su facebook'}),
        }

        error_messages = {
            NON_FIELD_ERRORS: {

                'nombre': {
                    'required': 'Por favor introduzca un nombre válido.'
                },
                'dni': {
                    'required': 'Por favor in introduzca un número de DNI válido.',
                },
                'email': {
                    'required': 'Por favor registre un email.',
                    'invalid': 'Este Email no esta permitido.',
                }
            }
        }

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get('email')

        if email == os.environ.get('MAIL_ADMIN'):
            raise forms.ValidationError('Este Email no esta permitido.')
