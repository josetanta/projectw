from django.shortcuts import render, HttpResponseRedirect
from django.db.models import Q

from django.contrib.auth.decorators import login_required

from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from django.views.decorators.csrf import csrf_protect

from django.urls import reverse_lazy

from ..usuario.models import User

from .mixins import SuperUserAndLoginMixin, BankAccountPassesTestMixin, ValidatePermissionStaffMixin, messages
from .forms import SocioForm
from .models import Socio, CuentaAhorro


# Create your views here.
# ------------------------


class SocioCreateView(SuperUserAndLoginMixin, ValidatePermissionStaffMixin, CreateView):
    model = Socio
    form_class = SocioForm
    template_name = 'apps/socios/crearSocio.html'
    success_url = reverse_lazy('socio:listasocio')
    permission_required = 'socio:crearsocio'

    def get_context_data(self, **kwargs):
        context = super(SocioCreateView, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        if form.is_valid():

            user = User(
                username=request.POST['dni'], email=request.POST['email'])
            user.set_password(request.POST['dni'].strip())
            user.save()
            form.instance.user_id = user.id
            form.instance.nombre = request.POST['nombre'].capitalize()
            form.instance.apellido_paterno = request.POST['apellido_paterno'].upper(
            )
            form.instance.apellido_materno = request.POST['apellido_materno'].upper(
            )
            form.save()
            messages.success(request, 'Se ha registrado un nuevo Socio.')
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form))


class SocioListView(SuperUserAndLoginMixin, ValidatePermissionStaffMixin, ListView):
    model = Socio
    template_name = 'apps/socios/listaSocio.html'
    context_object_name = 'socios'
    permission_required = 'socio:listasocio'


class SocioEditView(SuperUserAndLoginMixin, ValidatePermissionStaffMixin, UpdateView):
    model = Socio
    form_class = SocioForm
    context_object_name = 'socio'
    permission_required = 'socio:editarsocio'
    template_name = 'apps/socios/editarSocio.html'
    success_url = reverse_lazy('socio:listasocio')

    def get_context_data(self, **kwargs):
        context = super(SocioEditView, self).get_context_data(**kwargs)

        pk = self.kwargs.get('pk', 0)
        socio = self.model.objects.get(id=pk)
        user = self.second_model.objects.get(pk=socio.user_id)

        if 'form' not in context:
            context['form'] = self.form_class()

        return context


class SocioDeleteView(SuperUserAndLoginMixin, ValidatePermissionStaffMixin, DeleteView):
    model = Socio
    permission_required = 'socio:eliminarsocio'
    template_name = 'apps/socios/eliminarSocio.html'
    success_url = reverse_lazy('socio:listasocio')


class SocioDetailView(SuperUserAndLoginMixin, ValidatePermissionStaffMixin, DetailView):
    model = Socio
    template_name = 'apps/socios/socio_detail.html'
    permission_required = 'socio:detallesocio'
    context_object_name = 'socio'

    # Override
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        prestamos_aprobados = self.get_object().prestamos.filter(is_approved=True).all()
        prestamos_en_espera = self.get_object().prestamos.filter(is_approved=False).all()

        context['prestamos_aprobados'] = prestamos_aprobados
        context['prestamos_en_espera'] = prestamos_en_espera
        return context


class AccountBankDetailView(BankAccountPassesTestMixin, DetailView):
    model = CuentaAhorro
    context_object_name = 'bank_account'
    template_name = 'apps/bank_account/show.html'


@csrf_protect
@login_required
def search_partners(request):
    if request.method == 'GET':
        query = request.GET['query'].strip()
        resp = Socio.objects.filter(
            Q(dni=query)
        ).first()

        try:
            context = dict(socios=[resp])
        except:
            messages.error(request, 'Por favor no inserte ningun espacio')
            context = dict()

        return render(request, 'apps/socios/listaSocio.html', context=context)
