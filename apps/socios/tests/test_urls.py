from django.test import TestCase
from django.urls import reverse, resolve
from ...socios.views import SocioCreateView, SocioListView

class UrlsTestCase(TestCase):

    def test_list_url_is_resolved(self):
        url = reverse('socio:listasocio')
        self.assertEqual(resolve(url).func.view_class, SocioListView)

    def test_create_new_socio(self):
        url = reverse('socio:crearsocio')
        self.assertEqual(resolve(url).func.view_class, SocioCreateView)
