import datetime
from django.test import TestCase, Client
from django.urls import reverse
from ...usuario.models import User
from ...socios.models import Socio


class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.list_url = reverse('socio:editarsocio', args=[1, ])

        self.user = User.objects.create_user(
            username='jose1',
            email='jose1@mail.com'
        )
        self.socio = Socio.objects.create(
            id=1,
            nombre='jose',
            apellido_paterno='apellido',
            apellido_materno='apellido2',
            email='josehabck@gmail.com',
            dni='32312442',
            genero='M',
            fecha_nacimiento=datetime.datetime.now(),
            telefono='1234566789',
            user=self.user
        )

    def test_view_list_GET(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 302)

    def test_view_get_user(self):
        response = self.client.get(f'socios/detalle_socio/{self.socio.id}')

        self.assertEqual('jose' in response.context, False)
