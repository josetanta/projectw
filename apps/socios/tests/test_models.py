import datetime
from django.test import TestCase, Client
from ...socios.models import Socio, CuentaAhorro
from django.shortcuts import reverse


class SocioTestCase(TestCase):
      def _get_user(self):
            socio = Socio(
                  nombre = 'jose',
                  apellido_paterno = 'apellido',
                  apellido_materno = 'apellido2',
                  email = 'josehabck@gmail.com',
                  dni = '32312442',
                  genero = 'M',
                  fecha_nacimiento = datetime.datetime.now(),
                  telefono = '1234566789'
                  )
            return socio

      def test_validate_create_user(self):

            self.assertTrue(isinstance(self._get_user(), Socio))

      def test_it_create_validate_account_loan(self):

            ch = CuentaAhorro(monto_cuenta = 0, socio = self._get_user())

            self.assertTrue(isinstance(ch, CuentaAhorro))
            self.assertTrue(isinstance(ch.socio, Socio))
            self.assertEquals(ch.monto_cuenta, 0)