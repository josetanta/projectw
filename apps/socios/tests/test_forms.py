from datetime import datetime
from django.test import Client, TestCase, tag
from ...socios.forms import SocioForm
from ...socios.models import Socio, User


class FormsTestCase(TestCase):

    @tag('test_validate_form_SocioForm')
    def test_validate_form_SocioForm(self):
        partner = Socio(
            nombre='Jose',
            apellido_paterno='Tanta',
            apellido_materno='Calderon',
            dni='23234534',
            email='josetanta@joser.com',
            fecha_nacimiento=datetime(1996, 7, 2),
            genero='Masculino',
            telefono='123456789')
        user_data = User(username=partner.dni, email=partner.email)
        user_data.set_password(partner.dni)
        user_data.save()
        partner.user = user_data
        partner.save()

        data = dict(
            nombre=partner.nombre,
            apellido_paterno=partner.apellido_paterno,
            apellido_materno=partner.apellido_materno,
            dni=partner.dni,
            email=partner.email,
            fecha_nacimiento=partner.fecha_nacimiento,
            genero=partner.genero,
        )

        form = SocioForm(data=data)

        self.assertTrue(form.is_valid())
