from django.apps import AppConfig


class GestionsocioConfig(AppConfig):
    name = 'apps.socios'

    def ready(self):
        from .signals import save_account_bank_and_account, create_account_bank_and_account
