from django.dispatch import receiver
from django.db.models.signals import post_save
from .models import Socio, CuentaAhorro


@receiver(post_save, sender=Socio)
def create_account_bank_and_account(sender, instance, created, **kwargs):
    if created:
        CuentaAhorro.objects.create(monto_cuenta=0.0, socio=instance)


@receiver(post_save, sender=Socio)
def save_account_bank_and_account(sender, instance, **kwarg):
    instance.cuenta_ahorro.save()
