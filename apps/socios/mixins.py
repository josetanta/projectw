from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib import messages


class SuperUserAndLoginMixin:

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_staff:
                return super().dispatch(request, *args, **kwargs)

        messages.warning(request, 'Usted necesita inicar sesión.')
        return redirect('usuario:login')


class ValidatePermissionStaffMixin:
    permission_required = ('')
    url_redirect = None

    def get_perms(self):
        if isinstance(self.permission_required, str):
            return (self.permission_required)
        else:
            return self.permission_required

    def get_url_redirect(self):
        if self.url_redirect is None:
            return reverse_lazy('usuario:login')
        return self.url_redirect

    def dispatch(self, request, *args, **kwargs):
        '''
        Envio de petición en la vista
        '''
        if request.user.has_perms(self.get_perms()):
            return super().dispatch(request, *args, **kwargs)
        return self.get_url_redirect()


class BankAccountPassesTestMixin:

    def dispatch(self, request, *args, **kwargs):
        user = self.get_object()

        if request.user.socio.cuenta_ahorro.id != user.socio.cuenta_ahorro.id:
            return redirect('socio:account_bank', pk=request.user.socio.cuenta_ahorro.id)

        return super().dispatch(request, *args, **kwargs)
