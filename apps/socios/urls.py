from django.urls import path
from .views import (
    SocioCreateView,
    SocioDetailView,
    SocioListView,
    SocioEditView,
    SocioDeleteView,
    AccountBankDetailView,
    search_partners,
)

app_name = 'socio'
urlpatterns = [
    path('create-partner/', SocioCreateView.as_view(), name='crearsocio'),
    path('lists-partners/', SocioListView.as_view(), name='listasocio'),
    path('edit-partner/<int:pk>', SocioEditView.as_view(), name='editarsocio'),
    path('delete-partner/<int:pk>',
         SocioDeleteView.as_view(), name='eliminarsocio'),
    path('detail-partner/<int:pk>', SocioDetailView.as_view(), name='detallesocio'),
    path('<int:pk>/account-partner/',
         AccountBankDetailView.as_view(), name='account_bank'),
    path('search-partners/', search_partners, name='search-partners')
]
