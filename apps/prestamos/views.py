from datetime import datetime
from django.conf import settings
from django.shortcuts import get_object_or_404, HttpResponseRedirect, render
from django.urls import reverse

from django.db.models import Q

from django.contrib import messages
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    UserPassesTestMixin
)
from django.contrib.auth.decorators import login_required

from django.views.generic import ListView, CreateView, TemplateView
from django.views.decorators.csrf import csrf_protect

from .models import Prestamo
from .forms import PrestamoForm

from ..socios.models import Socio


# Create your views here.


class PrestamoListView(LoginRequiredMixin, ListView):
    model = Prestamo
    context_object_name = 'prestamos'
    template_name = 'apps/prestamos/listaPrestamo.html'
    ordering = ['-fecha_solicitud_prestamo']
    paginate_by = settings.PAGINATE_BY


class PrestamoCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Prestamo
    template_name = 'apps/prestamos/crearPrestamo.html'
    form_class = PrestamoForm

    def get_context_data(self, **kwargs):
        context = super(PrestamoCreateView, self).get_context_data(**kwargs)
        if self.request.user.socio.is_prestamos_max:
            context['warning'] = "Usted no puede realizar más solicitudes de prestamos."
            return context

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        if form.is_valid():
            form.instance.monto_consulta = form.cleaned_data.get(
                'monto_consulta', 0.0)
            form.instance.socio = request.user.socio
            form.save()
            messages.success(request, 'Usted acaba de solicitar un préstamo.')
            return HttpResponseRedirect(reverse('usuario:perfilusuario', args=[request.user.id, ]))

        else:
            messages.error(request, 'Revise los errores por favor.')
            return self.render_to_response(self.get_context_data(form=form))

    def test_func(self):
        socio_id = self.kwargs.get('pk', 0)
        if self.request.user.id == int(socio_id):
            return True
        return False


class PrestamoTemplateView(LoginRequiredMixin, TemplateView):
    model = Prestamo
    template_name = 'apps/usuario/prestamoListo.html'


@csrf_protect
@login_required
def approved_prestamo(request, id):
    prestamo = get_object_or_404(Prestamo, id=id)
    prestamo.approved = True
    prestamo.fecha_aprobacion_prestamo = datetime.now()
    prestamo.socio.cuenta_ahorro.add_amount_loan_approved(
        prestamo.monto_consulta)
    prestamo.save()
    messages.info(
        request,
        f'La solicitud del prestamo de {prestamo.socio.nombre} {prestamo.socio.apellido_paterno} {prestamo.socio.apellido_materno} fue aprobado ')
    return HttpResponseRedirect(reverse('prestamos:listaprestamo'))


@csrf_protect
@login_required
def cancel_request_loan(request, id):
    prestamo = get_object_or_404(Prestamo, id=id)
    prestamo.delete()
    messages.warning(request, 'Usted acaba de cancelar su solicitud')
    return HttpResponseRedirect(reverse('usuario:perfilusuario', args=[request.user.id, ]))


@csrf_protect
@login_required
def search_loans(request):
    if request.method == 'GET':
        query = request.GET.get('query')
        resp = Socio.objects.filter(
            Q(dni=query)
        ).first()

        try:
            context = dict(prestamos=resp.prestamos.all())
        except:
            messages.error(request, 'Por favor no inserte ningun espacio')
            context = dict()

        return render(request, 'apps/prestamos/listaPrestamo.html', context=context)
