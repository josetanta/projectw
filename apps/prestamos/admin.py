from django.contrib import admin
from .models import *
# Register your models here.


@admin.register(Prestamo)
class PrestamoAdmin(admin.ModelAdmin):
    search_fields = (
        'socio',
    )

    list_display = [
        'socio',
        'ocupacion_socio',
        'razon_prestamo',
        'fecha_solicitud_prestamo',
        'is_approved',
    ]
