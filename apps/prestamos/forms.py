from django import forms
from ckeditor.widgets import CKEditorWidget
from .models import Prestamo


class PrestamoForm(forms.ModelForm):
    monto_consulta = forms.DecimalField(
        max_value=4999, min_value=120, max_digits=7, decimal_places=2)

    class Meta:
        model = Prestamo

        fields = ['ocupacion_socio', 'razon_prestamo']

        labels = {
            'ocupacion_socio': 'Ocupación Laboral',
            'razon_prestamo': 'Razón de prestamo',
        }

        widgets = {
            'ocupacion_socio': forms.TextInput(attrs={'class': 'form-control'}),
            'razon_prestamo': CKEditorWidget(attrs={'class': 'form-control', 'id': 'razon'}),
        }

        error_messages = {
            'ocupacion_socio': {
                'invalid': "Debera completar es campo.",
            },
        }
