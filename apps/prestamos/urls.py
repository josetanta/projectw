from django.urls import path
from .views import (
	PrestamoListView,
	PrestamoCreateView,
	approved_prestamo,
	cancel_request_loan,
	search_loans
	)

app_name = 'prestamos'
urlpatterns = [
	path('registros_prestamos/',PrestamoListView.as_view(), name ='listaprestamo'),
	path('<int:pk>/solicitar_prestamo/', PrestamoCreateView.as_view(), name='solicitarprestamo'),
	path('<int:id>/approved_prestamo', approved_prestamo,name='approved-prestamo'),
	path('search-loans/', search_loans, name = 'search-loans'),
	path('<int:id>/', cancel_request_loan, name ='cancel_request_loan')
]