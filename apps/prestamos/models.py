from ckeditor.fields import RichTextField
from django.core.exceptions import ValidationError
from django.db import models

from ..socios.models import Socio


# Create your models here.

# CLASE DE PRESTAMOS , PARA CADA OSCIO EN LA SOLICITUD


class Prestamo(models.Model):
    id = models.BigAutoField(primary_key=True)
    ocupacion_socio = models.CharField('Ocupación laboral', max_length=50,
                                       blank=False, null=False, help_text='Profesión en el que se desempeña.')
    monto_consulta = models.DecimalField(
        'Monto a solicitar', blank=False, null=False, max_digits=7, decimal_places=2, default=0)
    razon_prestamo = RichTextField('Razones de Solicitud de préstamo',
                                   help_text='Su razón de solicitud debe de ser los más breve posible.')
    fecha_solicitud_prestamo = models.DateField(
        'Fecha de solicitud', auto_now_add=True)

    fecha_aprobacion_prestamo = models.DateField(
        'Fecha de aprobación', auto_now=False, null=True, blank=True)

    is_approved = models.BooleanField('Aprobado', default=False, null=False)
    socio = models.ForeignKey(
        Socio, on_delete=models.CASCADE, related_name='prestamos')

    class Meta:
        verbose_name = 'Prestamo'
        verbose_name_plural = 'Prestamos'
        db_table = 'prestamos'
        ordering = ['-fecha_solicitud_prestamo']
        # app_label = 'wfinanciera'

    def __str__(self):
        return "Prestamo %s" % self.socio

    @property
    def approved(self):
        return self.is_approved

    @approved.setter
    def approved(self, approved):
        self.is_approved = approved

    @property
    def get_mount_loan(self):
        return self.monto_consulta

    def clean(self):
        if self.monto_consulta > 9999.99:
            raise ValidationError('El monto debe de exceder de 10000 soles')
