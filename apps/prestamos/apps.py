from django.apps import AppConfig


class GestionprestamoConfig(AppConfig):
    name = 'apps.prestamos'
