from django.conf import settings

from django.shortcuts import reverse, render
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect

from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

from django.utils.decorators import method_decorator

from django.views.decorators.cache import never_cache
from django.views.generic import FormView, DetailView
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from django.core.mail import EmailMultiAlternatives

from django.template.loader import get_template

from .forms import FormularioLogin
from .mixins import AccountPassesTestMixin, LoginRequiredValidateUserMixin, messages
from .models import User
from ..prestamos.models import Prestamo


class LoginFormView(FormView):
    form_class = FormularioLogin
    template_name = 'apps/usuario/login.html'
    # Se dirige al perfil del usuario logeado
    success_url = reverse_lazy('inicio')

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(LoginFormView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        '''
        Validación del Form, y redireccionamiento del admin al Dashboard
        '''
        login(self.request, form.get_user())

        if self.request.user.is_admin:
            messages.info(self.request, 'Inicio Sesión de Staff')
            return HttpResponseRedirect(reverse('socio:listasocio'))

        messages.info(self.request, 'Inicio Sesión')
        return super(LoginFormView, self).form_valid(form)


@login_required
def logout_usuario(request):
    logout(request)
    return HttpResponseRedirect(reverse_lazy('usuario:login'))


class AccountDetailView(LoginRequiredValidateUserMixin, AccountPassesTestMixin, DetailView):
    model = User
    template_name = 'apps/usuario/account_detail.html'
    context_object_name = 'account'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if not self.request.user.is_admin:
            prestamos_aprobados = Prestamo.objects.filter(
                socio=self.request.user.socio, is_approved=True).all()
            prestamos_en_espera = Prestamo.objects.filter(
                socio=self.request.user.socio, is_approved=False).all()
            context['prestamos_aprobados'] = prestamos_aprobados
            context['prestamos_en_espera'] = prestamos_en_espera
            context['message'] = "<strong>Aviso: </strong>Usted ya no podrá realizar más solicitudes de Prestamo"
            return context
        return context


def send_email(mail):
    context = {'mail': mail}
    template = get_template('base/correo.html')
    content = template.render(context)

    email = EmailMultiAlternatives(
        'Un correo de prueba',
        'Admin W',
        settings.EMAIL_HOST_USER,
        [mail],
    )
    email.attach_alternative(content, 'text/html')
    email.send()


@csrf_exempt
def home(request):
    if request.method == 'POST':
        mail = request.POST['mail']
        send_email(mail)

    return render(request, 'base/main.html')


def about(request):
    return render(request, 'base/contactos.html')
