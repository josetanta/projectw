from django.urls import path
from .views import AccountDetailView, LoginFormView, logout_usuario

app_name = 'usuario'
urlpatterns = [
	path('partners/<int:pk>/', AccountDetailView.as_view(), name='perfilusuario'),
	path('partners/login/',LoginFormView.as_view(), name='login'),
	path('partners/logout/', logout_usuario, name='logout'),
]