from django.shortcuts import redirect
from django.contrib import messages


class AccountPassesTestMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            user = self.get_object()
            if request.user.id != user.id:
                return redirect('usuario:perfilusuario', pk=request.user.id)
        return super().dispatch(request, *args, **kwargs)


class LoginRequiredValidateUserMixin:

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.warning(request, 'Usted necesita inicar sesión.')
            return redirect('usuario:login')

        return super().dispatch(request, *args, **kwargs)
