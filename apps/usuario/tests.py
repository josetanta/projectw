from django.shortcuts import reverse
from django.test import TestCase, Client, tag

from .models import User


# Create your tests here.
class UsuarioTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User(username='jose', email='jose@mail.com')
        self.user.set_password('jose123')
        self.user.save()

        self.client.login(username=self.user.username,
                          password=self.user.password)

    def tearDown(self):
        self.client.logout()

    @tag('it_login_user_validate')
    def test_it_login_user_validate(self):
        response = self.client.post('/accounts/users/login/', {
            'username': 'jose',
            'password': 'jose123'
        })

        self.assertRedirects(response, reverse('inicio'), status_code=302)
        self.assertEqual(response.status_code, 302)

    @tag('it_login_validate_redirect_to_index')
    def test_it_login_validate_password(self):
        response = self.client.post('/accounts/users/login/', {
            'username': 'jose',
            'password': 'jose12313'
        })

        self.assertNotEqual(self.user.password, 'jose12313')
        self.assertContains(
            response,
            'Por favor, introduzca un Username y clave correctos. Observe que ambos campos pueden ser sensibles a mayúsculas')

    @tag('it_redirect_to_404')
    def test_it_redirect_to_404(self):
        response = self.client.get('/accounts/users/2/')
        self.assertEquals(response.status_code, 302)
        self.assertRedirects(response, reverse('usuario:login'))
