from django import forms
from django.contrib import admin
from django.contrib.auth.models import Permission
from django.contrib.auth.forms import ReadOnlyPasswordHashField
# Register your models here.
from .models import User


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Password Confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'username')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Las contraseñas no son iguales.')

        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'is_admin',
            'is_active',

        )

    def clean_password(self):
        return self.initial['password']


@admin.register(User)
class UserModelAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'is_staff',
                    'is_admin', 'is_superuser', 'is_active',)
    list_filter = ('is_admin', 'email', 'is_active')
    search_fields = ('email',)
    ordering = ('username',)
    # fieldsets = (
    #     (None, {'fields': ('username', 'email',
    #                        'password', 'groups',)}),
    #     ('Permisos', {'fields': ('is_admin', 'is_staff', 'is_superuser')}),
    # )


admin.site.register(Permission)
