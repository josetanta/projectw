from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
    Group
)
from django.db import models
from django.utils import timezone


class UserManager(BaseUserManager):

    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError('User has an email.')
        if not username:
            raise ValueError('User has an username')

        user = self.model(
            username=username,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(
            email=self.normalize_email(email),
            username=username,
        )
        user.set_password(password)
        user.is_staff = True
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField('Username', max_length=50, unique=True)
    email = models.EmailField('Email', max_length=100, unique=True)
    date_joined = models.DateTimeField(default=timezone.now)
    is_staff = models.BooleanField('Staff', default=False)
    is_admin = models.BooleanField('Admin', default=False)
    is_active = models.BooleanField('Active', default=True)
    groups = models.ManyToManyField(Group)
    objects = UserManager()

    class Meta:
        db_table = 'users'

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', ]

    def __str__(self):
        return "<User %s>" % self.username
