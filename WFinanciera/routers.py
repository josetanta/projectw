class HistoricRouter(object):
    """
    A router to control all database operations on models in the wfinanciera application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read wfinanciera models go to wfinanciera.
        """
        if model._meta.app_label == 'wfinanciera':
            return 'wfinanciera'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write wfinanciera models go to wfinanciera.
        """
        if model._meta.app_label == 'wfinanciera':
            return 'wfinanciera'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the wfinanciera app is involved.
        """
        if obj1._meta.app_label == 'wfinanciera' or \
                obj2._meta.app_label == 'wfinanciera':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the wfinanciera app only appears in the 'wfinanciera' database.
        """
        if app_label == 'wfinanciera':
            return db == 'wfinanciera'
        return None
