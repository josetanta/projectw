from django.contrib import admin
from django.http import HttpRequest
from django.urls import path, include
from django.contrib.auth.decorators import login_required
from apps.usuario import views as views_root

urlpatterns = [
    path('admin/', admin.site.urls),
    path('sobre-nosotros/', views_root.about, name='about'),
    path('', views_root.home, name='inicio'),
    path('partners/', include('apps.socios.urls')),
    path('loans/', include('apps.prestamos.urls')),
    path('accounts/', include('apps.usuario.urls')),
]
